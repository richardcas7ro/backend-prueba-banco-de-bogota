const Sequelize = require('sequelize');

const sequelizeConnection = require('./db.connection');


// Importar modelos

const LocationModel = require('../models/location.model');


//Inicializar modelos

const Location = LocationModel(sequelizeConnection,Sequelize);





const models = {
    Location : Location,
   
}
const db ={

    ...models,
    sequelizeConnection
}

module.exports= db;
