

const dbmanager = require('../database/dbmanager');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */

function crearlocacion(req,res){
    
    if(!req.body){
        res.status(400).send(
            {

            message: "Request esta vacio"

             }
        );
        return;
        
    }
    
    const newLocationObject ={
        username: req.body.username,
        area_m2: req.body.area_m2,
        
    }
    // Insertando en la abse de datos el objeto
    dbmanager.Location.create(newLocationObject).then(
        data => {
            res.send(data);
        }

    ).catch(
        error => {
            console.log(error);
            res.status (500).send(
                {
                    message: " ERRRRRROOOOOOOOOORRRRRRRR" + error

                }

            );
        }    
        
        
    );

}

async function findallLocations(req,res){
    try{const alllocations = await dbmanager.Location.findAll();

        res.send(
            {
                data: alllocations
            }
        );
    }catch(error){
        console.log(error)
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO"
            }
        )
    }
}

async function UpdateUserByUser(req,res){
    try{

        // se trae el parametro
       const { username } = req.params;

       const user = await dbmanager.User.findOne(
           {
               where: {
                   username: username }
           }
       ).then(function(obj) {
        // update
        if(obj)
            return obj.update(values);
        // insert
        return Model.create(values);
    });
        
        res.json(user);
        

    }catch(esrror){
        console.log(error,"Error")
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO,"
            }
        )

    }
}



async function DeleteByUsername (req,res) {
    const { username} = req.params;
    
   await dbmanager.Location.destroy({
        where: {
            username : username
        }
     })
     res.send(
         {
             message: "Eliminado"
         }
     )
    }
exports.crearlocacion = crearlocacion;
exports.findallLocations = findallLocations;
exports.UpdateLocationByUser = UpdateUserByUser;

exports.DeleteByUsername = DeleteByUsername;
