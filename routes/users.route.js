var express = require('express');
var router = express.Router();
const locationController = require('../controllers/locationController');



router.post('/', locationController.crearlocacion);
router.get('/',locationController.findallLocations);
router.delete('/:username',locationController.DeleteByUsername)
router.get('/:username',locationController.UpdateLocationByUser);



module.exports = router;
