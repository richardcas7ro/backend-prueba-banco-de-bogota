module.exports = (sequelize,Sequelize) => {
    const Location = sequelize.define('location',
        {
            idUser:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING,
                unique: true
                

            },
            area_m2: {
                type: Sequelize.DOUBLE,               

            }

        },{
            tableName: "Locations"
        }


    );

    return Location;

}